<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistEvent extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('artist_band', function (Blueprint $table) {
            $table->bigInteger('artist_id')->unsigned();
            $table->bigInteger('band_id')->unsigned();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
        
        Schema::create('band_event', function (Blueprint $table) {
            $table->bigInteger('band_id')->unsigned();
            $table->bigInteger('event_id')->unsigned();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('artist_band');
        Schema::dropIfExists('band_event');
    }
}