<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkInEventLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('event', function (Blueprint $table) {
            $table->index('location_id','location_id');
            $table->foreign('location_id')->references('id')
                ->on('location')->onDelete('cascade');
             });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
              Schema::table('event', function (Blueprint $table) {
        $table->dropForeign('event_location_id_foreign');
        $table->dropIndex('location_id');
    });
    }
}
