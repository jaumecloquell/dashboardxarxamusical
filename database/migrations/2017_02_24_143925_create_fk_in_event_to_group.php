<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkInEventToGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('event', function (Blueprint $table) {
            $table->index('group_id','group_id');
            $table->foreign('group_id')->references('id')
                ->on('group')->onDelete('cascade');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event', function (Blueprint $table) {
        $table->dropForeign('event_group_id_foreign');
        $table->dropIndex('group_id');
    });
    }
}
