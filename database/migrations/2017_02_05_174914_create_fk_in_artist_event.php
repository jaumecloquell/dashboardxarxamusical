<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkInArtistEvent extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::table('band_event', function (Blueprint $table) {
            $table->index('band_id','band_id');
            $table->foreign('band_id')->references('id')
            ->on('band')->onDelete('cascade');
            
            $table->index('event_id','event_id');
            $table->foreign('event_id')->references('id')
            ->on('event')->onDelete('cascade');
            
        });
        
        Schema::table('artist_band', function (Blueprint $table) {
            $table->index('artist_id','artist_id');
            $table->foreign('artist_id')->references('id')
            ->on('artist')->onDelete('cascade');
            
            $table->index('band_id','band_id');
            $table->foreign('band_id')->references('id')
            ->on('band')->onDelete('cascade');
            
        });
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::table('artist_band', function (Blueprint $table) {
            $table->dropForeign('artist_band_artist_id_foreign');
            $table->dropIndex('artist_id');
            
            $table->dropForeign('artist_band_band_id_foreign');
            $table->dropIndex('band_id');
        });

         Schema::table('band_event', function (Blueprint $table) {
            $table->dropForeign('band_event_band_id_foreign');
            $table->dropIndex('band_id');
            
            $table->dropForeign('band_event_event_id_foreign');
            $table->dropIndex('event_id');
        });
        
    }
    
}