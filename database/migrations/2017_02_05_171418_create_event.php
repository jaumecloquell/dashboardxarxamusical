<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('group', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });

         Schema::create('event', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('group_id')->unsigned();
            $table->bigInteger('location_id')->unsigned();
            $table->text('internal_description')->nullable();
            $table->text('external_description')->nullable();
            $table->integer('cost');
            $table->date('start_day');
            $table->date('end_day');
            $table->time('start_hour');
            $table->time('end_hour');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         
         Schema::dropIfExists('event');
         Schema::dropIfExists('group');
    }
}
