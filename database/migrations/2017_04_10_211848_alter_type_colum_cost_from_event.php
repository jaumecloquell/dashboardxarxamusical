<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\model\Event;

class AlterTypeColumCostFromEvent extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::table('event', function (Blueprint $table) {
            $table->decimal('decimal_cost')->default(0);
        });
        $events  = Event::All();
        if($events){
            foreach ($events as $event){
                $event->decimal_cost = $event->cost;
                $event->save();
            }
        }

        Schema::table('event', function (Blueprint $table) {
            $table->renameColumn('cost' , 'temporal_cost');
        });

        Schema::table('event', function (Blueprint $table) {
            $table->string('temporal_cost')->default(0)->change();
        });
       
         Schema::table('event', function (Blueprint $table) {
            $table->renameColumn('decimal_cost' , 'cost');
        });
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::table('event', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('event', function (Blueprint $table) {
            $table->renameColumn('temporal_cost', 'cost');
        });
       

        


    }
}