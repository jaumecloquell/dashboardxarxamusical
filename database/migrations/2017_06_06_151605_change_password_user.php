<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePasswordUser extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        DB::table('users')
        ->where('email', 'admin@xarxamusical.com')
        ->update(['password' => Hash::make('pepxarxamusical')]);
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        DB::table('users')
        ->where('email', 'admin@xarxamusical.com')
        ->update(['password' => Hash::make('pw1403sha1')]);
    }
}