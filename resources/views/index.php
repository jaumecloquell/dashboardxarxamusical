<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Xarca Musical</title>
  <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">


  <!-- Custom CSS -->
  <link href="css/sb-admin.css" rel="stylesheet">

  <!-- Morris Charts CSS -->
  <link href="css/plugins/morris.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css/multipleDatePicker.css" rel="stylesheet" type="text/css">
  <link href="style/style.css" rel="stylesheet" type="text/css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body ng-app="authApp">
  <div ui-view></div>
</body>

<!-- Application Dependencies -->
<script src="node_modules/angular/angular.js"></script>
<script src="node_modules/angular-ui-router/build/angular-ui-router.js"></script>
<script src="node_modules/satellizer/satellizer.js"></script>
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

<link rel="stylesheet" ; href="https://unpkg.com/ng-table@2.0.2/bundles/ng-table.min.css">
<script src="https://unpkg.com/ng-table@2.0.2/bundles/ng-table.min.js"></script>

<!-- Application Scripts -->
<script src="scripts/app.js"></script>
<script src="scripts/authController.js"></script>
<script src="scripts/userController.js"></script>
<script src="scripts/dashboard/homeController.js"></script>
<script src="scripts/dashboard/artistListController.js"></script>
<script src="scripts/dashboard/artistDetailController.js"></script>
<script src="scripts/dashboard/eventListController.js"></script>
<script src="scripts/dashboard/eventDetailController.js"></script>
<script src="scripts/dashboard/locationListController.js"></script>
<script src="scripts/dashboard/locationDetailController.js"></script>
<script src="scripts/dashboard/bandListController.js"></script>
<script src="scripts/dashboard/bandDetailController.js"></script>

<!-- services -->
<script src="scripts/services/event.service.js"></script>
<script src="scripts/services/artist.service.js"></script>
<script src="scripts/services/band.service.js"></script>
<script src="scripts/services/location.service.js"></script>

<!-- Morris Charts JavaScript -->
<script src="js/plugins/morris/raphael.min.js"></script>
<script src="js/plugins/morris/morris.min.js"></script>
<script src="js/plugins/morris/morris-data.js"></script>
<script src="js/plugins/calendar/scripts/daypilot-all.min.js" type="text/javascript"></script>
<script src="js/multipleDatePicker.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="js/moment-timezone.js"></script>

</html>