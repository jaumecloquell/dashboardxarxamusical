<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
</head>
<body>
<style>
body {
    font-size: 62.5%;
}
</style>
<?php  
$daysName = ["Monday", "Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
$spanishDays = ["Lunes", "Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"];

$dateParams = new DateTime($startDay);
$today = $dateParams->format("d");
$month = $dateParams->format("m");
$year = $dateParams->format("Y");
$days = cal_days_in_month(CAL_GREGORIAN,$month,$year);
// Days in current month
$lastmonth = date("t", mktime(0,0,0,$month-1,1,$year));
// Days in previous month
$start = date("N", mktime(0,0,0,$month,1,$year));
// Starting day of current month
$finish = date("N", mktime(0,0,0,$month,$days,$year));
// Finishing day of  current month
$laststart = $start - 1;
// Days of previous month in calander
$counter = 1;
$nextMonthCounter = 1;
$thisMonth = false;
if($start > 5){
	$rows = 6;
}
else{
	$rows = 5;
}
?>
<h2 style="text-align:center">XARXA MUSICAL</h2>
<h3 style="text-align:center">{{$location->name}}</h3>
<p style="text-align:center">{{$startDay}} / {{$endDay}}</p>
<table width="100%" border="1" bordercolor="#cccccc" cellpadding="5" cellspacing="5">
	<tr style="text-align:center">
    @foreach($spanishDays as $d)
        <th>
            {{$d}}
        </th>
    @endforeach
    </tr>
    @for($i = 0; $i < $rows; $i++)
        <tr>
            @for($x = 0; $x < 7; $x++)
                @if(($counter - $start) < 0)
                    <?php  
                    $date = (($lastmonth - $laststart) + $counter);
$thisMonth = false;
$style = "background:grey"
                     ?>
                @elseif (($counter - $start) >= $days)  
                     <?php 
                      $date = ($nextMonthCounter);
$nextMonthCounter++;
$thisMonth = false;
$style = "background:grey"
                      ?>
                @else
                     <?php 
                        $date = ($counter - $start + 1);
$thisMonth = true;
$style = "text-align:center;"
                      ?>     
                @endif
                     <td style={{$style}}>
                        @if($thisMonth)
                            <div style="text-align: right; top:0;">
                                <sup>{{$date}}</sup>
                             </div>
                            @foreach($events as $event)
                                <?php  
                                    $actualDay = date('j', strtotime($event->start_day));
                                ?>
                                @if($actualDay == $date)
                                    <hr>
                                    @foreach($event->artists as $artist)
                                       * <b>{{$artist->name}}</b> *
                                        @if(count($event->artists) > 1)
                                          <br> 
                                        @endif 
                                    @endforeach
                                    <br>
                                    
                                    @if($needCost != 'false')
                                        <a>{{$event->cost}} €</a> 
                                     @endif 
                                    <br>    
                                @endif
                        @endforeach
                      @endif  
                   </td>  
                   <?php 
                        $counter++
                    ?> 
             @endfor
        </tr>
     @endfor   
</table>    
<br>
<?php  
if($needCost != 'false'){
	$pricetotal = 0;
	foreach($events as $event){
		$pricetotal += $event->cost;
	}
}
?>
@if($needCost != 'false')
<h3>TOTAL : {{$pricetotal}} €</h3> 
@endif 
<!--div style="text-align:center">
    <h3>Xarxa Musical</h3>
    <p>Poligono Son Fuster, Palma</p>
    <p>971 791 231</p>
    <p>info@xarxamusical.com - wwww.xarxamusical.com</p>
</div-->
</body>
</html>