<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
</head>
<body>
<?php  
$days = ["Monday", "Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
$spanishDays = ["Lunes", "Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"];
$costTotal = 0;
$date = new DateTime($start);
$week = $date->format("W");
$month = $date->format("F");
$year = $date->format("Y");
?>
<h1 style="text-align:center">XARXA MUSICAL</h1>
<h2>PLANNING FROM {{$start}} TO {{$end}}</h2>
<h2>{{$artist->name}}</h2>
<table width="100%" border="1" bordercolor="#cccccc" cellpadding="5" cellspacing="5">
    <tr style="text-align:center">
    @foreach($spanishDays as $d)
        <th>
            {{$d}}
        </th>
    @endforeach
    </tr>
    <tr>
    @foreach($days as $d)
      <td style="text-align:center">
        @foreach($events as $event)
            <?php  
                $day = date('l', strtotime($event->start_day));
            ?>
                @if($d == $day)
                    <hr>
                    <b>{{$event->name }}</b> <br> 
                    {{$event->city}} / ({{$event->address}})<br>
                    {{ $event->start_hour }} / {{ $event->end_hour }} <br>
                    {{ $event->external_description }} <br>   
                    <hr><br>
                @endif
        @endforeach
        </td>
    @endforeach
    </tr>
</table>
<br>
<div style="text-align:center">
    <h3>Xarxa Musical</h3>
    <p>Poligono Son Fuster, Palma</p>
    <p>971 791 231</p>
    <p>administracion@xarxamusical.com - comercial@xarxamusical.com</p>
    <p>www.xarxamusical.com</p>
</div>
</body>
</html>