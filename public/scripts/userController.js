(function() {

	'use strict';

	angular
		.module('authApp')
		.controller('UserController', UserController);

	function UserController($http, $auth, $rootScope, $state) {

		var vm = this;
		vm.user = $rootScope.currentUser;
		vm.openModal = openModal;
		vm.closeModal = closeModal;
		vm.saveButton = saveButton;
		vm.editButton = editButton;
		vm.infoDisabled = true;

		function editButton() {
			if (vm.infoDisabled == true) {
				vm.infoDisabled = false;
			}else{
				vm.infoDisabled = true;
			}
		}

		function saveButton(){
			var params =  {
				'name' : vm.user.name,
				'email': vm.user.email
			}
			$http.post('api/user/' + vm.user.id, params).success(function (user) {
				vm.user = user;
				closeModal();
			}).error(function (error) {
				alert("Se ha producido un error al actualizar el usuario");
			});
		}

		function openModal(){
			$('#mySettingModal').modal('show');
		}
		function closeModal(){
			$('#mySettingModal').modal('hide');
		}
		// We would normally put the logout method in the same
		// spot as the login method, ideally extracted out into
		// a service. For this simpler example we'll leave it here
		vm.logout = function() {

			$auth.logout().then(function() {

				// Remove the authenticated user from local storage
				localStorage.removeItem('user');

				// Flip authenticated to false so that we no longer
				// show UI elements dependant on the user being logged in
				$rootScope.authenticated = false;

				// Remove the current user info from rootscope
				$rootScope.currentUser = null;

				// Redirect to auth (necessary for Satellizer 0.12.5+)
				$state.go('auth');
			});
		}
	}
	
})();