(function () {

	'use strict';

	angular
		.module('authApp')
		.controller('BandListController', BandListController);

	function BandListController($http, $auth, $rootScope, $state, bandService, NgTableParams, artistService) {
		var vm = this;
		vm.goToDetails = goToDetails;
		vm.createBand = createBand;
		vm.closeModal = closeModal;
		vm.name = "";

		start()
		function start() {
			getBands()
			getArtists()
		}

		function goToDetails(id) {
			$state.go("users.bandDetail", { 'id': id });
		}

		function closeModal() {
			$('#myModalBand').modal('hide');
		}

		function checkParams(arrayArtist) {
			var correct = true;

			if (vm.name == "") {
				alert("Introduce un nombre al grupo")
				correct = false;
			}
			if (arrayArtist.length == 0) {
				alert("Asigna como mínimo un artista al grupo")
				correct = false;
			}

			return correct;
		}

		function createBand() {

			var arrayArtist = [];
			angular.forEach(vm.artists, function (value, key) {
				if (value.selected == true) {
					arrayArtist.push(value.id);
				}
			});

			if (checkParams(arrayArtist)) {
				var params = {
					'name': vm.name,
					'artists': arrayArtist
				}

				bandService.createBand(params)
					.success(function (band_id) {
						closeModal()
						goToDetails(band_id);
					}).error(function (error) {
						alert("Error al crear el grupo");
					});
			}
		}

		function getArtists() {
			artistService.getArtistList()
				.success(function (artists) {
					vm.artists = artists;
					vm.artists.forEach(function (artist) {
						artist.selected = false;
					});
					vm.artistTableParams = new NgTableParams({}, { dataset: vm.artists });
				}).error(function (error) {
					alert("Error al obtener la lista de artistas")
				});
		}

		function getBands() {
			bandService.getBandList()
				.success(function (bands) {
					vm.bands = bands;
					vm.tableParams = new NgTableParams({}, { dataset: vm.bands });
				}).error(function (error) {
					alert("Error al obtener la lista de bandas")
				});
		}
	}

})();