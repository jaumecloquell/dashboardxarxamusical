(function () {

	'use strict';

	angular
		.module('authApp')
		.controller('LocationListController', LocationListController);

	function LocationListController($http, $auth, $rootScope, $state, NgTableParams, locationService) {
		var vm = this;
		vm.goToDetails = goToDetails;
		vm.closeModal = closeModal;
		vm.chooseCity = chooseCity;
		vm.createLocation = createLocation;

		vm.name = null;
		vm.address = null;
		vm.citySelected = null;



		start()
		function start() {
			getCityList();
			initLocation()
		}
		function chooseCity(city) {
			vm.citySelected = city;
		}

		function closeModal() {
			$('#myModal').modal('hide');
		}

		function initLocation() {
			locationService.getLocationList()
				.success(function (locations) {
					vm.locations = locations;
					vm.locationTableParams = new NgTableParams({}, { dataset: vm.locations });
				}).error(function (error) {
					alert("Se ha producido un error al obtener las localizaciones");
				});
		}

		function goToDetails(id) {
			$state.go("users.locationDetail", { 'id': id });
		}


		function getCityList() {
			$http.get('api/city').success(function (city) {
				vm.city = city;
				vm.cityTableParams = new NgTableParams({}, { dataset: vm.city });
			}).error(function (error) {
				alert("Se ha producido un error al obtener las ciudades");
			});
		}

		function checkParams() {
			if (vm.name != null) {
				if (vm.address != null) {
					if (vm.citySelected != null) {
						return true;
					} else {
						alert("Selecciona una ciudad");
					}
				} else {
					alert("Introduce una dirección");
				}
			} else {
				alert("Introduce un nombre");
			}
			return false;
		}

		function createLocation() {
			var params = {
				'name': vm.name,
				'address': vm.address,
				'city_id': vm.citySelected.id
			}
			if (checkParams()) {
				locationService.createLocation(params)
					.success(function (location) {
						closeModal();
						initLocation();
					}).error(function (error) {
						alert("Se ha producido un error al crear la localización");
					});
			}
		}
	}

})();