(function () {

	'use strict';

	angular
		.module('authApp')
		.controller('EventListController', EventListController);

	function EventListController($http, $auth, $rootScope, $state, $scope, NgTableParams, eventService, artistService, bandService, locationService) {

		var vm = this;
		
		vm.getPDFBandMonth = getPDFBandMonth;
		vm.getPDF = getPDF;
		vm.getPDFBand = getPDFBand;
		vm.createEvent = createEvent;
		vm.typeCalendar = "Month";
		vm.closeModal = closeModal;
		vm.chooseLocation = chooseLocation;
		vm.applyRepeatEvents = applyRepeatEvents;
		vm.showEvent = showEvent;
		/**Variables necesarias para crear un evento*/
		vm.typeRepetition = 14;
		vm.endDateRepetition = null;

		vm.selectedDates;
		vm.actualDate;
		vm.startHour;
		vm.endHour;
		vm.internal_description = null;
		vm.external_description = null;
		vm.cost = 0;
		vm.locationSelected = null;
		vm.addCost = false;




		start();

		function start() {
			//initParams();
			initEvents();
			calendar();
			getLocations();
			getBands();
		}
		function getPDFBandMonth(){
			location.href = '/api/bandListPDFMonth/' + vm.start + '/' + vm.end
		}
		//generación de la carpeta de pdf de las bandas
		function getPDFBand(){
			location.href = '/api/bandListPDF/' + vm.start + '/' + vm.end
		}

		function getPDF() {
			location.href = '/api/locationListPDF/' + vm.start + '/' + vm.end + '/' + vm.addCost
			
		}

		function restartParams() {
			vm.endDateRepetition = null;
			vm.selectedDates = [];
			vm.actualDate = null;
			vm.startHour = null;
			vm.endHour = null;
			vm.internal_description = null;
			vm.external_description = null;
			vm.cost = 0;
			vm.locationSelected = null;

			vm.band.forEach(function (band) {
				band.selected = false;
			});
		}

		/**Función que nos ayudará a realizar la repeticion de eventos en el calendario*/
		function applyRepeatEvents() {
			var correct = true;
			if (vm.selectedDates.length == 0) {
				alert("Selecciona una fecha de inicio");
				correct = false;
			}

			if (vm.endDateRepetition == null) {
				alert("Introduce una una fecha final");
				correct = false;
			}

			if (!moment(vm.endDateRepetition).isValid()) {
				alert("Introduce la fecha final correctamente, por ejemplo 2017-02-22");
				correct = false;
			}
			if (correct) {
				var actualDate = vm.selectedDates[vm.selectedDates.length - 1];
				var next = moment(actualDate).add(vm.typeRepetition, 'DAYS');
				while (next.isSameOrBefore(moment(vm.endDateRepetition))) {
					vm.selectedDates.push(next);
					var actualDate = vm.selectedDates[vm.selectedDates.length - 1];
					var next = moment(actualDate).add(vm.typeRepetition, 'DAYS');
				}
			}
		}

		/*Funcion adicional utilizada para seleccionar una localizacion*/
		function chooseLocation(location) {
			vm.locationSelected = location;
			console.log(vm.locationSelected);
		}

		function closeModal() {
			restartParams()
			$('#myModal').modal('hide');
		}

		/*Obtenemos los eventos de este mes*/
		function initEvents() {
			//getMonth() == 0 -> enero, == 1 -> febrero
			var today = new Date();
			var firstDay = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + '1';
			var lastDay = today.getFullYear() + '-' + (today.getMonth() + 2) + '-' + '1';

			getEvent(firstDay, lastDay);

		}

		/*Obtenemos todos las localizaciones disponibles*/
		function getLocations() {
			locationService.getLocationList()
				.success(function (locations) {
					vm.locations = locations;
					vm.locationTableParams = new NgTableParams({}, { dataset: vm.locations });
				}).error(function (error) {
					alert("Se ha producido un error al obtener las localizaciones");
				});
		}

		/*Obtenemos todos los artistas disponibles*/
		function getBands() {
			bandService.getBandList()
				.success(function (band) {
					vm.band = band;
					vm.band.forEach(function (band) {
						band.selected = false;
					});

					vm.bandTableParams = new NgTableParams({}, { dataset: vm.band });
				}).error(function (error) {
					alert("Error al descargar todos las bandas");
				});
		}

		/*Detecta cambios en la variable vm.typeCalendar para poder actualizar el calendario*/
		$scope.$watch(angular.bind(vm, function () {
			return vm.typeCalendar;
		}), function (newVal) {
			calendar();
		});

		/*Función que prepara la configuración del calendario*/
		function calendar() {
			vm.navigatorConfig = {
				//weekStarts: 1,
				locale: "es-es",
				selectMode: vm.typeCalendar,
				showMonths: 3,
				skipMonths: 3,
				onTimeRangeSelected: function (args) {
					var selectedYear = args.day.getYear();
					vm.start = args.start.toString("yyyy-MM-dd");
					vm.end = args.end.toString("yyyy-MM-dd");
					vm.end = moment(vm.end).add(-1, 'DAYS').format("YYYY-MM-DD") //eliminamos 1 dia porque siempre el 
					getEvent(vm.start, vm.end);
					vm.CalendarConfig.startDate = args.day;
				}
			};

			vm.CalendarConfig = {
				//weekStarts: 1,
				locale: "es-es",
				timeFormat: "Clock24Hours",
				viewType: vm.typeCalendar,
				onEventClick: function (args) {
					showEvent(args.e.data.id);
				},
				onEventResized: function (args) {
					updateEvent(args);
				},
				onEventMoved: function (args) {
					updateEvent(args);
				},
				onTimeRangeSelected: function (args) {

				}
			};
		}

		/*Función que cmabio el estado de la vista*/
		function showEvent(id) {
			$state.go("users.eventDetail", { 'id': id });
		}

		/*Función que comprueba que hemos llenado todos los imputs necesarios*/
		function checkInput(arrayDates, arrayBand) {
			var result = true;
			if (arrayDates.length == 0) {
				result = false;
				alert("El evento tiene que tener como mínimo una fecha asignada");
			}

			if (arrayBand.length == 0) {
				result = false;
				alert("Asigna como mínimo a un banda al evento");
			}

			var startHour = moment(vm.startHour, "h:mm");
			var endHour = moment(vm.endHour, "h:mm");
			if (!startHour.isValid() || !endHour.isValid()) {
				result = false;
				alert("Introduce un horario al evento correctamente");
			}

			if (!startHour.isSameOrBefore(endHour)) {
				result = false;
				alert("Start Hour debe ser anterior a End Hour");
			}

			if (vm.locationSelected == null) {
				result = false;
				alert("Selecciona una localización");
			}
			return result;
		}

		/**Obtiene todos los valores necesarios para crear un evento */
		function createEvent() {
			var arrayDates = [];
			var arrayBand = [];
			var location;
			/**Recorre el array de fechas dadas para obtener el formato correcto para nuestra bbdd */
			angular.forEach(vm.selectedDates, function (value, key) {
				arrayDates.push(value.format("YYYY-MM-DD"));
			});
			/*Recorre el array de artistas para saber cual de ellos a sido seleccionado*/
			angular.forEach(vm.band, function (value, key) {
				if (value.selected == true) {
					arrayBand.push(value.id);
				}
			});

			/**Obetenemos la localizacion que hemos seleccionado */
			location = vm.locationSelected;

			if (checkInput(arrayDates, arrayBand)) {
				var params = {
					'internal_description': vm.internal_description,
					'external_description': vm.external_description,
					'dates': arrayDates,
					'start_hour': vm.startHour,
					'end_hour': vm.endHour,
					'bands': arrayBand,
					'location': location.id,
					'cost': vm.cost
				}

				eventService.createEvent(params)
					.success(function (events) {
						closeModal();
						if (events.length == 1) {
							showEvent(events[0]);
						} else {
							initEvents();
						}
					}).error(function (error) {
						alert("Se ha producido un error al crear el evento");
					});
			}
		}

		/**Pepara los parametros para actualizar el evento */
		function updateEvent(args) {
			var params = {
				'start_hour': args.newStart.toString("HH:mm:ss"),
				'end_hour': args.newEnd.toString("HH:mm:ss"),
				'start_day': args.newStart.toString("yyyy-MM-dd"),
				'end_day': args.newEnd.toString("yyyy-MM-dd"),
				'id': args.e.data.id,
				'internal_description': args.e.data.internal_description,
				'external_description': args.e.data.external_description,
				'cost': args.e.data.cost,
				'location_id': args.e.data.location_id
			}

			eventService.updateEvent(params)
				.success(function (events) {

				}).error(function (error) {
					alert("Se ha producido un error al actualizar el evento");
				});
		}


		/*Función que obtienen todos los eventos que se encuentran entre dos fechas dadas*/
		function getEvent(start, end) {
			eventService.getEventList(start, end).success(function (events) {
				vm.events = events;

				if (vm.typeCalendar == "Day") {
					vm.eventsTableParams = new NgTableParams({}, { dataset: vm.events });
				}
				calendar();
			}).error(function (error) {
				alert("Se ha producido un error al obtener los eventos");
			});
		}

	}

})();