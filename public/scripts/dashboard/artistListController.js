(function () {

	'use strict';

	angular
		.module('authApp')
		.controller('ArtistListController', ArtistListController);
	function ArtistListController($http, $auth, $rootScope, $state, NgTableParams, $window, artistService) {

		var vm = this;
		vm.create = create;
		vm.goToDetails = goToDetails;
		vm.name = null;
		vm.email = null;
		vm.telf = null;
		vm.closeModal = closeModal;

		start();

		function start() {
			getArtists();
		}
		function closeModal() {
			$('#myModal').modal('hide');
		}

		function checkParams() {
			if (vm.name && vm.email && vm.telf) {
				return true;
			} else {
				alert("Rellena todos los campos");
				return false;
			}
		}

		function create() {
			if (checkParams()) {
				var data = {
					'name': vm.name,
					'email': vm.email,
					'telf': vm.telf
				};
				artistService.createArtist(data)
					.success(function (artist_id) {
						closeModal();
						goToDetails(artist_id);
					}).error(function (error) {
						alert("Error al crear un nuevo artista");
					});
			} else {
				$window.alert("¡Introduce todos los campos correctamente!");
			}
		}

		function goToDetails(id) {
			$state.go("users.artistDetail", { 'id': id });
		}

		function getArtists() {
			artistService.getArtistList()
				.success(function (users) {
					vm.users = users;
					vm.tableParams = new NgTableParams({}, { dataset: vm.users });
				}).error(function (error) {

				});
		}

	}

})();