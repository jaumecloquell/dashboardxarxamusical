(function () {

	'use strict';

	angular
		.module('authApp')
		.controller('ArtistDetailController', ArtistDetailController);
	function ArtistDetailController($scope, $http, $auth, $rootScope, $state, NgTableParams, $window, $stateParams, eventService, artistService) {

		var vm = this;
		vm.typeCalendar = "Month";
		vm.infoDisabled = true;
		vm.OverlapEvents = [];
		vm.editButton = editButton;
		vm.saveButton = saveButton;
		vm.deleteArtist = deleteArtist;
		vm.getPDF = getPDF;

		start();

		function start() {
			initEvents();
			getArtistInfo();
			calendar();

		}

		function initEvents() {
			//getMonth() == 0 -> enero, == 1 -> febrero
			var today = new Date();
			vm.start = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + '1';
			vm.end = today.getFullYear() + '-' + (today.getMonth() + 2) + '-' + '1';
			getEvents(vm.start, vm.end);
		}

		function goToArtistList() {
			$state.go("users.artist");
		}
		
		/**Función que elimina un artista*/
		function deleteArtist() {
			var id = $stateParams.id;
			artistService.deleteArtist(id)
				.success(function (users) {
					goToArtistList();
				}).error(function (error) {
					alert("Error al eliminar el artista");
				});
		}

		$scope.$watch(angular.bind(vm, function () {
			return vm.typeCalendar;
		}), function (newVal) {
			calendar();
		});

		function calendar() {
			vm.navigatorConfig = {
				//weekStarts: 1,
				locale: "es-es",
				selectMode: vm.typeCalendar,
				showMonths: 3,
				skipMonths: 3,
				onTimeRangeSelected: function (args) {
					vm.CalendarConfig.startDate = args.day;
					vm.start = args.start.toString("yyyy-MM-dd");
					vm.end = args.end.toString("yyyy-MM-dd");
					getEvents(vm.start, vm.end);
				}
			};
			vm.CalendarConfig = {
				//weekStarts: 1,
				locale: "es-es",
				ShowToolTip: true,
				viewType: vm.typeCalendar,
				onEventClick: function (args) {
					showEvent(args.e.data.id);
				},
				onEventResized: function (args) {
					updateEvent(args);
				},
				onEventMoved: function (args) {
					updateEvent(args);
				},
				onTimeRangeSelected: function (args) {

				}
			};
		}

		function showEvent(id) {
			$state.go("users.eventDetail", { 'id': id });
		}
		/**Prepara las variables para actualizar el evento */
		function updateEvent(args) {
			var params = {
				'start_hour': args.newStart.toString("HH:mm:ss"),
				'end_hour': args.newEnd.toString("HH:mm:ss"),
				'start_day': args.newStart.toString("yyyy-MM-dd"),
				'end_day': args.newEnd.toString("yyyy-MM-dd"),
				'id': args.e.data.id,
				'internal_description': args.e.data.internal_description,
				'external_description': args.e.data.external_description,
				'cost': args.e.data.cost,
				'location_id': args.e.data.location_id
			}
			eventService.updateEvent(params)
				.success(function (events) {
					checkOverlapEvents();
				}).error(function (error) {
					alert("Se ha producido un error al actualizar el evento");
				});
		}

		function getArtistInfo() {
			var id = $stateParams.id;
			artistService.getArtist(id)
				.success(function (data) {
					vm.artist = data.artist;
					vm.bands = data.bands;
				}).error(function (error) {
				});
		}

		function getEvents(start, end) {
			var params = {
				'params': {
					'start': start,
					'end': end
				},
				'id': $stateParams.id
			}

			artistService.getArtistEvents(params)
				.success(function (events) {
					vm.events = events;
					checkOverlapEvents();
					calendar();
				}).error(function (error) {
					alert("Se ha producido un error al obtener los evento");
				});
		}

		function editButton() {
			if (vm.infoDisabled == true) {
				vm.infoDisabled = false;
			} else {
				vm.infoDisabled = true;
			}
		}

		//actualiza el artista
		function saveButton() {
			vm.infoDisabled = true;
			var params = {
				'name': vm.artist.name,
				'telf': vm.artist.telf,
				'email': vm.artist.email,
				'id': vm.artist.id
			}
			artistService.updateArtist(params)
				.success(function (artist) {
					vm.artist = artist;
				}).error(function (error) {
					alert("Se ha producido un error al actualizar el artista");
				});
		}

		function getPDF() {
			var id = $stateParams.id;
			var end = moment(vm.end).add(-1, 'DAYS').format("YYYY-MM-DD") //eliminamos 1 dia porque siempre el calendario nos coge un dia de mas
			location.href = '/api/artistPDF/' + id + '/' + vm.start + '/' + end
		}

		function checkOverlapEvents() {
			var params = {
				'params': {
					'start': vm.start,
					'end': vm.end
				},
				'id': $stateParams.id
			}
			artistService.checkOverlapEvents(params).success(function (data) {
				vm.OverlapEvents = data;
			}).error(function (error) {
				alert("Error al comprobar solapamiento de eventos");
			});
		}

	}

})();