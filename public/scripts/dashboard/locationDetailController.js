(function () {

	'use strict';

	angular
		.module('authApp')
		.controller('LocationDetailController', LocationDetailController);

	function LocationDetailController($http, $auth, $rootScope, $state, NgTableParams, $stateParams, $scope, eventService, locationService) {
		var vm = this;
		vm.typeCalendar = "Month";
		vm.editButton = editButton;
		vm.infoDisabled = true;
		vm.saveButton = saveButton;
		vm.getPDF = getPDF;
		vm.needPricePDF = false;

		start()
		function start() {
			initEvents();
			calendar();
			getLocationInfo();
		}

		

		function getPDF() {
			var id = $stateParams.id;
			var end = moment(vm.end).add(-1, 'DAYS').format("YYYY-MM-DD") //eliminamos 1 dia porque siempre el 
			location.href = '/api/locationPDF/' + id + '/' + vm.start + '/' + end + '/' + vm.needPricePDF
			
		}

		function getEvent(start, end) {
			var id = $stateParams.id;
			var params = {
				'params': {
					'start': start,
					'end': end
				},
				'id': id
			}
			locationService.getLocationEvent(params)
				.success(function (events) {
					vm.events = events;
					calendar();
				}).error(function (error) {
					alert("Se ha producido un error al obtener los eventos");
				});
		}

		function initEvents() {
			//getMonth() == 0 -> enero, == 1 -> febrero
			var today = new Date();
			vm.start = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + '1';
			vm.end = today.getFullYear() + '-' + (today.getMonth() + 2) + '-' + '1';
			getEvent(vm.start, vm.end);
		}

		$scope.$watch(angular.bind(vm, function () {
			return vm.typeCalendar;
		}), function (newVal) {
			calendar();
		});

		function calendar() {
			vm.navigatorConfig = {
				locale: "es-es",
				selectMode: vm.typeCalendar,
				showMonths: 3,
				skipMonths: 3,
				onTimeRangeSelected: function (args) {
					var selectedYear = args.day.getYear();
					vm.start = args.start.toString("yyyy-MM-dd");
					vm.end = args.end.toString("yyyy-MM-dd");
					getEvent(vm.start, vm.end);
					vm.CalendarConfig.startDate = args.day;
				}
			};

			vm.CalendarConfig = {
				locale: "es-es",
				timeFormat: "Clock24Hours",
				viewType: vm.typeCalendar,
				onEventClick: function (args) {
					showEvent(args.e.data.id);
				},
				onEventResized: function (args) {
					updateEvent(args);
				},
				onEventMoved: function (args) {
					updateEvent(args);
				},
				onTimeRangeSelected: function (args) {
				}
			};
		}

		function updateEvent(args) {

			var params = {
				'start_hour': args.newStart.toString("HH:mm:ss"),
				'end_hour': args.newEnd.toString("HH:mm:ss"),
				'start_day': args.newStart.toString("yyyy-MM-dd"),
				'end_day': args.newEnd.toString("yyyy-MM-dd"),
				'id': args.e.data.id,
				'internal_description': args.e.data.internal_description,
				'external_description': args.e.data.external_description,
				'cost': args.e.data.cost,
				'location_id': args.e.data.location_id
			}
			eventService.updateEvent(params)
				.success(function (events) {

				}).error(function (error) {
					alert("Se ha producido un error al actualizar el evento");
				});
		}

		function showEvent(id) {
			$state.go("users.eventDetail", { 'id': id });
		}

		function editButton() {
			if (vm.infoDisabled == true) {
				vm.infoDisabled = false;
			} else {
				vm.infoDisabled = true;
			}
		}

		function saveButton() {
			var params = {
				'name': vm.location.name,
				'address': vm.location.address,
				'city_id': vm.location.city_id,
				'id': $stateParams.id
			}
			editButton();

			locationService.updateLocation(params)
				.success(function (location) {

				}).error(function (error) {
					alert("Se ha producido un error al actualizar la localizacion");
				});

		}

		function getLocationInfo() {
			var id = $stateParams.id;
			locationService.getLocation(id)
				.success(function (location) {
					vm.location = location.location;
					vm.city = location.city;
				}).error(function (error) {
					alert("Se ha producido un error al obtener la localizacion");
				});
		}
	}

})();