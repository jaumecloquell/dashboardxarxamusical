(function () {

	'use strict';

	angular
		.module('authApp')
		.controller('BandDetailController', BandDetailController);

	function BandDetailController($http, $auth, $rootScope, $state, bandService, $stateParams, NgTableParams, $scope, eventService) {
		var vm = this;
		vm.typeCalendar = "Month";
		vm.OverlapEvents = [];

		vm.showEvent = showEvent;
		vm.infoDisabled = true;
		vm.editButton = editButton;
		vm.saveButton = saveButton;
		vm.assignArtist = assignArtist;
		vm.deleteArtist = deleteArtist;
		vm.visitArtist = visitArtist;
		vm.deleteBand = deleteBand;
		vm.getPDF = getPDF;

		start()
		function start() {
			initBand();
			initEvents();
		}

		function getPDF() {
			var id = $stateParams.id;
			//var end = moment(vm.end).add(-1, 'DAYS').format("YYYY-MM-DD") //eliminamos 1 dia porque siempre el calendario nos coge un dia de mas
			location.href = '/api/bandPDF/' + id + '/' + vm.start + '/' + vm.end
		}

		function deleteBand() {

			bandService.deleteBand($stateParams.id)
				.success(function (data) {
					$state.go("users.bands");
				}).error(function (error) {
					alert("Error al eliminar")
				});
		}

		function initEvents() {
			//getMonth() == 0 -> enero, == 1 -> febrero
			var today = new Date();
			vm.start = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + '1';
			vm.end = today.getFullYear() + '-' + (today.getMonth() + 2) + '-' + '1';
			getEvents(vm.start, vm.end);
		}

		function visitArtist(id) {
			$state.go("users.artistDetail", { 'id': id });
		}

		function assignArtist(id) {
			var params = {
				'artist': id,
				'id': $stateParams.id
			}

			bandService.assignArtist(params)
				.success(function (data) {
					initBand();
				}).error(function (error) {
					alert("Error al obtener la información de la banda")
				});
		}

		function deleteArtist(id) {
			var params = {
				'artist': id,
				'id': $stateParams.id
			}

			bandService.deleteArtist(params)
				.success(function (data) {
					initBand();
				}).error(function (error) {
					alert("Error al obtener la información de la banda")
				});
		}

		function initBand() {
			var id = $stateParams.id;
			bandService.getBand(id)
				.success(function (data) {
					vm.band = data.band;
					vm.artists = data.artists;
					vm.restArtists = data.restArtists;
					vm.artistTableParams = new NgTableParams({}, { dataset: vm.artists });
					vm.restArtistTableParams = new NgTableParams({}, { dataset: vm.restArtists });
				}).error(function (error) {
					alert("Error al obtener la información de la banda")
				});
		}

		function saveButton() {
			vm.infoDisabled = true;
			var id = $stateParams.id;
			var params = {
				'name': vm.band.name,
				'id': id
			}
			bandService.updateBand(params)
				.success(function (band) {
					vm.band = band;
				}).error(function (error) {
					alert("Se ha producido un error al actualizar el grupo");
				});
		}

		function editButton() {
			if (vm.infoDisabled == true) {
				vm.infoDisabled = false;
			} else {
				vm.infoDisabled = true;
			}
		}

		//configuracion del calendario//
		$scope.$watch(angular.bind(vm, function () {
			return vm.typeCalendar;
		}), function (newVal) {
			calendar();
		});


		function showEvent(id) {
			$state.go("users.eventDetail", { 'id': id });
		}

		function checkOverlapEvents() {
			var params = {
				'params': {
					'start': vm.start,
					'end': vm.end
				},
				'id': $stateParams.id
			}
			bandService.checkOverlapEvents(params).success(function (data) {
				vm.OverlapEvents = data;
			}).error(function (error) {
				alert("Error al comprobar solapamiento de eventos");
			});
		}

		function getEvents(start, end) {
			var params = {
				'params': {
					'start': start,
					'end': end
				},
				'id': $stateParams.id
			}

			bandService.getBandEvents(params)
				.success(function (events) {

					if (vm.typeCalendar == "Day") {
						vm.eventsTableParams = new NgTableParams({}, { dataset: vm.events });
					}
					vm.events = events;
					checkOverlapEvents();
					calendar();
				}).error(function (error) {
					alert("Se ha producido un error al obtener los evento");
				});
		}

		function calendar() {
			vm.navigatorConfig = {
				//weekStarts: 1,
				locale: "es-es",
				ShowToolTip: true,
				selectMode: vm.typeCalendar,
				showMonths: 3,
				skipMonths: 3,
				onTimeRangeSelected: function (args) {
					vm.CalendarConfig.startDate = args.day;
					vm.start = args.start.toString("yyyy-MM-dd");
					vm.end = args.end.toString("yyyy-MM-dd");
					vm.end = moment(vm.end).add(-1, 'DAYS').format("YYYY-MM-DD") //eliminamos 1 dia porque siempre el 
					getEvents(vm.start, vm.end);
				}
			};
			vm.CalendarConfig = {
				//weekStarts: 1,
				locale: "es-es",
				ShowToolTip: true,
				viewType: vm.typeCalendar,

				onEventClick: function (args) {
					showEvent(args.e.data.id);
				},
				onEventResized: function (args) {
					updateEvent(args);
				},
				onEventMoved: function (args) {
					updateEvent(args);
				},
				onTimeRangeSelected: function (args) {

				}
			};
		}

		function showEvent(id) {
			$state.go("users.eventDetail", { 'id': id });
		}

		function updateEvent(args) {
			var params = {
				'start_hour': args.newStart.toString("HH:mm:ss"),
				'end_hour': args.newEnd.toString("HH:mm:ss"),
				'start_day': args.newStart.toString("yyyy-MM-dd"),
				'end_day': args.newEnd.toString("yyyy-MM-dd"),
				'id': args.e.data.id,
				'internal_description': args.e.data.internal_description,
				'external_description': args.e.data.external_description,
				'cost': args.e.data.cost,
				'location_id': args.e.data.location_id
			}
			eventService.updateEvent(params)
				.success(function (events) {
					checkOverlapEvents();
				}).error(function (error) {
					alert("Se ha producido un error al actualizar el evento");
				});
		}




	}

})();