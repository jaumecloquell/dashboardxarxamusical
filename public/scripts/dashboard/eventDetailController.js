(function () {

	'use strict';

	angular
		.module('authApp')
		.controller('EventDetailController', EventDetailController);
	function EventDetailController($scope, $http, $auth, $rootScope, $state, NgTableParams, $window, $stateParams, eventService, locationService) {

		var vm = this;
		vm.infoDisabled = true;
		vm.editButton = editButton;
		vm.saveButton = saveButton;
		vm.deleteBand = deleteBand;
		vm.visitBand = visitBand;
		vm.deleteEvent = deleteEvent;
		vm.assignBand = assignBand;
		vm.visitLocation = visitLocation;
		vm.chooseLocation = chooseLocation;
		vm.spread = false;
		start();

		function start() {
			getEventInfo();
			getAllLocations();
		}
		/*Función que se utiliza para cambiar el estado de la vista */
		function visitLocation() {
			$state.go("users.locationDetail", { 'id': vm.location.id });
		}

		/*Obtenemos toda la información del evento actual*/
		function getEventInfo() {
			var id = $stateParams.id;
			eventService.getEvent(id)
				.success(function (data) {
					vm.restBand = data.restBand
					vm.band = data.band
					vm.event = data.event;
					vm.location = data.location;
					vm.bandTableParams = new NgTableParams({}, { dataset: vm.band });
					vm.restBandTableParams = new NgTableParams({}, { dataset: vm.restBand });
				}).error(function (error) {
					alert("Error al obtener los datos del evento");
				});
		}

		/**Asignamos un nuevo artista al evento actual*/
		function assignBand(band_id) {
			console.log("assign");
			var id = $stateParams.id;
			var params = {
				'band_id': band_id,
				'event_id': id,
				'spread': vm.spread
			}
			eventService.assignBand(params).success(function (data) {
				getEventInfo();
			}).error(function (error) {
				alert("Error al eliminar el artista del evento");
			});
		}


		function visitBand(band_id) {
			$state.go("users.bandDetail", { 'id': band_id });
		}


		function editButton() {
			if (vm.infoDisabled == true) {
				vm.infoDisabled = false;
			} else {
				vm.infoDisabled = true;
			}
		}

		function chooseLocation(location) {
			vm.event.location_id = location.id;
		}

		function closeModal() {
			$('#myModal').modal('hide');
		}

		/**Funcion que prepara los parámetros para actualizar un evento*/
		function saveButton() {
			var params = {
				'start_hour': vm.event.start_hour,
				'end_hour': vm.event.end_hour,
				'start_day': vm.event.start_day,
				'end_day': vm.event.end_day,
				'internal_description': vm.event.internal_description,
				'external_description': vm.event.external_description,
				'cost': vm.event.cost,
				'location_id': vm.event.location_id,
				'spread': vm.spread,
				'id': $stateParams.id
			}

			eventService.updateEvent(params)
				.success(function (data) {
					getEventInfo();
				}).error(function (error) {
					alert("Error al actualizar el evento");
				});

			editButton(); //bloqueamos los imputs
			closeModal(); //cerramos el modal abiero
		}

		/**Elimina un artista al evento actual*/
		function deleteBand(band_id) {
			var id = $stateParams.id;
			var params = {
				'band_id': band_id,
				'event_id': id,
				'spread': vm.spread
			}

			eventService.deleteBand(params)
				.success(function (data) {
					
					getEventInfo();
				}).error(function (error) {
					alert("Error al eliminar el artista del evento");
				});
		}

		/**Eliminamos el evento actual*/
		function deleteEvent() {
			var params = {
				'params': {
					'spread': vm.spread
				}
			}
			var id = $stateParams.id;
			eventService.deleteEvent(id, params).success(function (data) {
				$state.go("users.events");
			}).error(function (error) {
				alert("Error al eliminar el evento");
			});
		}

		/**Obtenemos todas las localizaciones disponibles*/
		function getAllLocations() {
			locationService.getLocationList()
				.success(function (locations) {
					vm.locations = locations;
					vm.locationTableParams = new NgTableParams({}, { dataset: vm.locations });
				}).error(function (error) {
					alert("Error al obtener las localizaciones");
				});
		}
		
	}

})();