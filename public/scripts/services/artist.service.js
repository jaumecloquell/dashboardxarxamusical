// service
angular
    .module('authApp')
    .service('artistService', artistService);

function artistService($http) {

    this.getArtist = function (id) {
        return $http.get('api/artist/' + id)
    }

    this.getArtistList = function () {
        return $http.get('api/artist')
    }

    this.deleteArtist = function (id) {
        return $http.delete('api/artist/' + id)
    }

    this.createArtist = function (params) {
        return $http.put('api/artist', params);
    }

    this.updateArtist = function (params) {
        return $http.post('api/artist/' + params.id, params);
    }

    this.getArtistEvents = function (params) {
        return $http.get('api/artist/' + params.id + '/events', params)
    }

    this.checkOverlapEvents = function (params) {
        return $http.get('api/artist/' + params.id + '/checkOverlapEvents', params)
    }

}