// service
angular
    .module('authApp')
    .service('locationService', locationService);

function locationService($http) {

    this.getLocationList = function () {
        return $http.get('api/location');
    }

    this.createLocation = function (params) {
        return $http.put('api/location', params);
    }

    this.updateLocation = function (params) {
        return $http.post('api/location/' + params.id, params)
    }

    this.getLocation = function (id) {
        return $http.get('api/location/' + id);
    }

    this.getLocationEvent = function (params) {
        return $http.get('api/location/' + params.id + '/events', params)
    }
}