// service
angular
    .module('authApp')
    .service('eventService', eventService);

function eventService($http) {

    this.getEvent = function (id) {
        return $http.get('api/event/' + id);
    }

    this.getEventList = function (start, end) {
        var params = {
            'params': {
                'start': start,
                'end': end
            }
        }
        return $http.get('api/event', params);
    }

    this.updateEvent = function (params) {
        return $http.post('api/event/' + params.id, params);
    }

    this.createEvent = function (params) {
        return $http.put('api/event', params);
    }

    this.deleteEvent = function (id, params) {
        return $http.delete('api/event/' + id, params);
    }

    this.deleteBand = function (params) {
        return $http.post('api/eventband', params);
    }

    this.assignBand = function (params) {
        return $http.post('api/eventband/' + params.event_id, params);
    }

}