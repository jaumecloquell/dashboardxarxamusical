// service
angular
    .module('authApp')
    .service('bandService', bandService);

function bandService($http) {

    this.getBand = function (id) {
        return $http.get('api/band/' + id)
    }

    this.assignArtist = function (params) {
        return $http.post('api/artistband/' + params.id, params);
    }

     this.deleteArtist = function (params) {
        return $http.post('api/artistband/', params);
    }

    this.getBandList = function () {
        return $http.get('api/band')
    }

    this.deleteBand = function (id) {
        return $http.delete('api/band/' + id)
    }

    this.createBand = function (params) {
        return $http.put('api/band', params);
    }

    this.updateBand = function (params) {
        return $http.post('api/band/' + params.id, params);
    }

    this.getBandEvents = function (params) {
        return $http.get('api/band/' + params.id + '/events', params)
    }

    this.checkOverlapEvents = function (params) {
        return $http.get('api/band/' + params.id + '/checkOverlapEvents', params)
    }

}