<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\model\Event;

class EventController extends Controller
{
	
	public function __construct()
	    {
		$this->middleware('jwt.auth');
	}
	
	public function delete(Request $request, $id){
		if ($request->has('spread')){
			$deleteRest = $request->input('spread');
		}
		else{
			$deleteRest = false;
		}
		if($deleteRest == "true"){
			$event = Event::findOrFail($id);
			return $event->deleteRestEvents();
		}
		else{
			return Event::destroy($id);
		}
		
	}
	
	
	/**
	* Return the event
	     *
	     * @return Response
	     */
	    public function index(Request $request)
	    {
	
		$this->validate($request, [
		            'start' => 'required|date_format:"Y-m-d',
		            'end' => 'required|date_format:"Y-m-d'
		         ]);
		
		$params = [
		            'start' => $request->input('start'),
		            'end' => $request->input('end'),
		        ];
		return Event::getEventsCalendar($params);
	}
	
	
	/**
	* Return the event
	     *
	     * @return Response
	     */
	    public function create(Request $request)
	    {
		$this->validate($request, [
		            'location' => 'required|numeric',
		            'dates' => 'required|array',
		            'bands' => 'required|array',
		            'internal_description' => 'sometimes',
		            'external_description' => 'sometimes',
		            'start_hour' => 'required',
		            'cost' => 'required|numeric',
		            'end_hour' => 'required'
		       ]);
		
		$params = [
		            'location' => $request->input('location'),
		            'dates' => $request->input('dates'),
		            'bands' => $request->input('bands'),
		            'start_hour' => $request->input('start_hour'),
		            'end_hour' => $request->input('end_hour'),
		            'internal_description' => $request->input('internal_description'),
		            'external_description' => $request->input('external_description'),
		            'cost' => $request->input('cost')
		        ];
		return Event::createEvent($params);
		
	}
	
	
	public function assignBand(Request $request, $id){
		$this->validate($request, [
		            'band_id' => 'required|numeric',
		            'event_id' => 'required|numeric'
		       ]);
		$params = [
		            'band_id' => $request->input('band_id'),
		            'event_id' => $request->input('event_id')
		        ];
		$event = Event::findOrFail($id);
		
		if ($request->has('spread')){
			$assignRest = $request->input('spread');
		}
		else{
			$assignRest = false;
		}
		
		if($assignRest == "true"){
			$event->assignRestBand($params);
		}
		else{
			$event->assignBand($params);
		}
		
		
	}
	
	public function deleteBand(Request $request){

	
		$this->validate($request, [
		            'band_id' => 'required|numeric',
		            'event_id' => 'required|numeric'
		       ]);
		$params = [
		            'band_id' => $request->input('band_id'),
		            'event_id' => $request->input('event_id')
		        ];
		$event = Event::findOrFail($params['event_id']);
		
		if ($request->has('spread')){
			$deleteRest = $request->input('spread');
		}
		else{
			$deleteRest = false;
		}
		
		if($deleteRest == "true"){
			return $event->deleteRestBand($params);
		}
		else{
			return $event->deleteBand($params);
		}
	}
	
	
	/**
	* Return the event
	     *
	     * @return Response
	     */
	    public function updateEvent(Request $request, $id)
	    {
		$this->validate($request, [
		            'start_day' => 'required|date|date_format:Y-m-d',
		            'end_day' => 'required|date|date_format:Y-m-d',
		            'start_hour' => 'required',
		            'end_hour' => 'required',
		            'internal_description' => 'sometimes',
		            'external_description' => 'sometimes',
		            'location_id' => 'required|numeric',
		            'cost' => 'required|numeric',
		       ]);
		$params = [
		            'start_day' => $request->input('start_day'),
		            'end_day' => $request->input('end_day'),
		            'start_hour' => $request->input('start_hour'),
		            'end_hour' => $request->input('end_hour'),
		            'internal_description' => $request->input('internal_description'),
		            'external_description' => $request->input('external_description'),
		            'location_id' => $request->input('location_id'),
		            'cost' => $request->input('cost')
		        ];
		if ($request->has('spread')){
			$params['spread'] = $request->input('spread');
		}
		else{
			$params['spread'] = false;
		}
		
		$event = Event::findOrFail($id);
		if($request['spread'] == "true"){
			return  $event->updateRestEvents($params);
		}
		else{
			return  $event->updateEvent($params);
		}
	}
	
	public function show(Request $request, $id){
		$event = Event::findOrFail($id);
		return  $event->show();
	}
	
	
	
}
