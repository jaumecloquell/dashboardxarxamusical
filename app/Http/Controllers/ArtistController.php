<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\model\Artist;
use PDF;
use Validator;

class ArtistController extends Controller
{
    
    public function __construct()
    {
        /*$this->middleware('jwt.auth', ['only' => ['updateArtist','index', 'delete', 'getEvents', 'show', 'create']]);*/
        $this->middleware('jwt.auth', ['except' => ['createPdf']]);
        
    }
    
    public function createPdf(Request $request, $id, $start, $end){
        $rules = array(
        'id'            => 'required|numeric',
        'start'             => 'required|date_format:"Y-m-d',
        'end'            => 'required|date_format:"Y-m-d'
        );
        
        $urlParam = [
        'start' => $start,
        'end' => $end,
        'id' => $id
        ];

        $validator = Validator::make($urlParam, $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return $messages;
            
        }else{

            $artist = Artist::findOrFail($id);
            $params = [
            'start' => $start,
            'end' => $end
            ];
            $events = $artist->getEvents($params);
            
            $htmlParams = [
            'start' => $start,
            'end' => $end,
            'events' => $events,
            'artist' => $artist
            ];
            $pdf = PDF::loadView('calendarEvents', $htmlParams)->setPaper('a4', 'landscape');
            //return $pdf->stream('xarxaMusical.pdf');
            return $pdf->download($artist->name.'_'.$start.'.pdf');
        }
        
    }
    
    
    /**
    * Return the artists
    *
    * @return Response
    */
    public function index()
    {
        return  Artist::all();
        
    }
    
    
    /**
    * Return the artist
    *
    * @return Response
    */
    public function delete(Request $request, $id)
    {
        Artist::destroy($id);
    }
    
    
    public function checkOverlapEvents(Request $request, $id){
        $this->validate($request, [
        'start' => 'required|date_format:"Y-m-d',
        'end' => 'required|date_format:"Y-m-d'
        ]);
        
        $params = [
        'start' => $request->input('start'),
        'end' => $request->input('end'),
        ];
        $artist = Artist::findOrFail($id);
        return $artist->checkOverlapEvents($params);
    }
    
    /**
    * Return the events
    *
    * @return Response
    */
    public function getEvents(Request $request, $id){
        $this->validate($request, [
        'start' => 'required|date_format:"Y-m-d',
        'end' => 'required|date_format:"Y-m-d'
        ]);
        
        $params = [
        'start' => $request->input('start'),
        'end' => $request->input('end'),
        ];
        $artist = Artist::findOrFail($id);
        return $artist->getEventsCalendar($params);
        
    }
    
    
    
    /**
    * Return the artist
    *
    * @return Response
    */
    public function show(Request $request, $id)
    {
        $artist = Artist::findOrFail($id);
        return  $artist->show();
    }
    
    /**
    * Return the artist
    *
    * @return Response
    */
    public function create(Request $request)
    {
        $this->validate($request, [
        'name' => 'required|string',
        'email' => 'required|string',
        'telf' => 'required|numeric',
        ]);
        
        $params = [
        'name' => $request->input('name'),
        'email' => $request->input('email'),
        'telf' => $request->input('telf')
        ];
        return Artist::createArtist($params);
        
    }
    
    
    /**
    * Return the artist
    *
    * @return Response
    */
    public function updateArtist(Request $request, $id)
    {
        $this->validate($request, [
        'name' => 'required|string',
        'email' => 'required|string',
        'telf' => 'required|numeric',
        ]);
        
        $params = [
        'name' => $request->input('name'),
        'email' => $request->input('email'),
        'telf' => $request->input('telf')
        ];
        $artist = Artist::findOrFail($id);
        return $artist->updateArtist($params);
    }
    
    
}