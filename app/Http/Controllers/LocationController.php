<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\model\Location;
use PDF;
use Validator;


class LocationController extends Controller
{
    
    public function __construct()
    {
        /*$this->middleware('jwt.auth', ['only' => ['index', 'create', 'getEvents', 'show', 'update']]);*/
        $this->middleware('jwt.auth', ['except' => ['createPdf', 'getListPdf']]);
    }
    /*Dado una serie de parametros, generamos una lista de archivos pdf, que guardaremos en local, para después generar un zip que contenha todo ello*/
    public function getListPdf(Request $request,$start, $end, $needCost = false){
        $rules = array(
        'start'             => 'required|date_format:"Y-m-d',
        'end'            => 'required|date_format:"Y-m-d'
        );
        
        $urlParam = [
        'start' => $start,
        'end' => $end,
        ];
        
        $validator = Validator::make($urlParam, $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return $messages;
            
        }else{
            //eliminamos temporales archivos generados en anteriores peticiones
            $localDirectory = "../storage/pdf/pdfLocations/";
            $zipName = "../storage/pdf/pdfLocations.zip";
            $file_list = array_diff(scandir($localDirectory), array('..', '.'));
            
            //eliminos los pdf
            foreach($file_list as $file){
                unlink($localDirectory.$file);
            }
            //eliminamos el zip final
            if (file_exists($zipName)) {
                unlink($zipName);
            }
            
            //obtenemos un listado de todos los hoteles junto sus respectivos eventos para dicho mes
            $locationList = Location::getLocationsByTime($urlParam);
            
            //preparamos los parámetros para generar los pdf
            foreach($locationList as $location){
                
                $htmlParams = [
                'startDay' => $start,
                'endDay' => $end,
                'events' => $location->events,
                'location' => $location,
                'needCost' => $needCost
                ];
                
                //guardamos los pdf
                $pdf = PDF::loadView('locationCalendarEvents', $htmlParams)->setPaper('a4', 'landscape');
                $pdf->save($localDirectory.$location->name.'_'.$start.'.pdf');
            }
            //Generamos el zip ha descargar
            $zip = new \ZipArchive();
            $file_list = array_diff(scandir($localDirectory), array('..', '.'));

            if($file_list){
                if ($zip->open($zipName, \ZipArchive::CREATE) === true) {
                    foreach ($file_list as $file) {
                        $zip->addFile($localDirectory.$file, $file);
                    }
                    
                    $zip->close();
                    //forzamos las descarga del zip
                    //header("Content-type: application/octet-stream");
                    //header("Content-disposition: attachment; filename=locationsList_".$start.".zip");
                    // readfile($zipName);
                    return response()->download($zipName, 'locationsList_'.$start.'.zip');
                }
            }else{
                return "No existen eventos";
            }
        }
    }
    
    /*Dado una serie de parámetros, descargamos, sin guardar en local, un archivo pdf que contiene los eventos de un hotel en concreto */
    public function createPdf(Request $request, $id, $start, $end, $needCost, $download = false){
        
        $rules = array(
        'id'            => 'required|numeric',
        'start'             => 'required|date_format:"Y-m-d',
        'end'            => 'required|date_format:"Y-m-d',
        'needCost'            => 'required'
        );
        
        $urlParam = [
        'start' => $start,
        'end' => $end,
        'id' => $id,
        'needCost' => $needCost
        ];
        
        $validator = Validator::make($urlParam, $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return $messages;
            
        }else{
            
            $location = Location::findOrFail($id);
            $params = [
            'start' => $start,
            'end' => $end
            ];
            $events = $location->getEvents($params);
            
            $htmlParams = [
            'startDay' => $start,
            'endDay' => $end,
            'events' => $events,
            'location' => $location,
            'needCost' => $needCost
            ];
            
            $pdf = PDF::loadView('locationCalendarEvents', $htmlParams)->setPaper('a4', 'landscape');
            //return $pdf->stream('xarxaMusical.pdf');
            if($download == false){
                return $pdf->download($location->name.'_'.$start.'.pdf');
            }else{
                return $pdf->save($location->name.'_'.$start.'.pdf')->download($location->name.'_'.$start.'.pdf');
            }
        }
        
        
    }
    
    
    
    /**
    * Return the event
    *
    * @return Response
    */
    public function index()
    {
        return Location::getAll();
        
    }
    
    
    /**
    * Return the event
    *
    * @return Response
    */
    public function create(Request $request)
    {
        $this->validate($request, [
        'name' => 'required|string',
        'address' => 'required|string',
        'city_id' => 'required|numeric'
        ]);
        $params = [
        'name' => $request->input('name'),
        'address' => $request->input('address'),
        'city_id' => $request->input('city_id')
        ];
        return Location::createLocation($params);
        
    }
    
    public function show(Request $request, $id){
        $location = location::findOrFail($id);
        return  $location->showLocation();
        
    }
    
    public function getEvents(Request $request, $id){
        $this->validate($request, [
        'start' => 'required',
        'end' => 'required'
        
        ]);
        $params = [
        'start' => $request->input('start'),
        'end' => $request->input('end'),
        
        ];
        $location = location::findOrFail($id);
        return  $location->getEventsCalendar($params);
        
    }
    
    
    /**
    * Return the event
    *
    * @return Response
    */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
        'name' => 'required|string',
        'address' => 'required|string',
        'city_id' => 'required|numeric'
        ]);
        $params = [
        'name' => $request->input('name'),
        'address' => $request->input('address'),
        'city_id' => $request->input('city_id')
        ];
        $location = location::findOrFail($id);
        return  $location->updateLocation($params);
    }
    
    
    
}