<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\model\City;

class CityController extends Controller
{
	
	public function __construct()
	    {
		$this->middleware('jwt.auth');
	}
	
	
	/**
	* Return the event
	     *
	     * @return Response
	     */
	    public function index()
	    {
		return City::All();
	}
	
	
	/**
	* Return the event
	     *
	     * @return Response
	     */
	    public function create(Request $request)
	    {
		$this->validate($request, [
		            'name' => 'required',
		            'code' => 'required'
		         ]);
		$params = [
		            'name' => $request->input('name'),
		            'code' => $request->input('code')
		        ];
		return City::createCity($params);
		
	}
	
	public function show(Request $request, $id){
		$city = City::findOrFail($id);
		return  $city;
	}
	
	
	/**
	* Return the event
	     *
	     * @return Response
	     */
	    public function update(Request $request, $id)
	    {
		$this->validate($request, [
		            'name' => 'required',
		            'code' => 'required'
		
		]);
		$params = [
		            'name' => $request->input('name'),
		            'code' => $request->input('code')
		        ];
		$city = City::findOrFail($id);
		return  $city->updateCity($params);
	}
	
	
	
}
