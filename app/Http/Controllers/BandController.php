<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\model\Band;
use PDF;
use Validator;


class BandController extends Controller
{
    
    public function __construct()
    {
        
        $this->middleware('jwt.auth', ['except' => ['createPdf', 'getListPdf','getListMonthPdf']]);
        
    }
    
    public function getListMonthPdf(Request $request,$start, $end){
        $rules = array(
        'start'             => 'required|date_format:"Y-m-d',
        'end'            => 'required|date_format:"Y-m-d'
        );
        
        $urlParam = [
        'start' => $start,
        'end' => $end,
        ];
        
        $validator = Validator::make($urlParam, $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return $messages;
            
        }else{
            //eliminamos temporales archivos generados en anteriores peticiones
            $localDirectory = "../storage/pdf/pdfBands/";
            $zipName = "../storage/pdf/pdfBands.zip";
            $file_list = array_diff(scandir($localDirectory), array('..', '.'));
            
            //eliminos los pdf
            foreach($file_list as $file){
                unlink($localDirectory.$file);
            }
            //eliminamos el zip final
            if (file_exists($zipName)) {
                unlink($zipName);
            }
            
            //$last = new \DateTime($urlParam['end']);
            $textdt=$urlParam['start'];
            $dt= strtotime( $textdt);
            $currdt=$dt;
            $nextmonth=strtotime($textdt."+1 month");
            $i=0;
            do
            {
                $weekday= date("w",$currdt);
                $nextday=7-$weekday;
                $endday=abs($weekday-7);
                $startarr[$i]=$currdt;
                $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
                $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");
                //echo "Week ".($i+1). "-> start date = ". date("Y-m-d",$startarr[$i])." end date = ". date("Y-m-d",$endarr[$i])."<br>";
                //--- generamos los pdf---
                $param = [
                'start' => date("Y-m-d",$startarr[$i]),
                'end' => date("Y-m-d",$endarr[$i]),
                ];
                $bandList = Band::getBandsByTime($param);
                
                //preparamos los parámetros para generar los pdf
                foreach($bandList as $band){
                    
                    $htmlParams = [
                    'start' => $param['start'],
                    'end' => $param['end'],
                    'events' => $band->events,
                    'artist' => $band
                    ];
                    
                    //guardamos los pdf
                    $pdf = PDF::loadView('calendarEvents', $htmlParams)->setPaper('a4', 'landscape');
                    $pdf->save($localDirectory.$band->name.'_'.$param['start'].'.pdf');
                }
                $i++;
                
            }while($endarr[$i-1]<$nextmonth);
            //Generamos el zip ha descargar
                $zip = new \ZipArchive();
                $file_list = array_diff(scandir($localDirectory), array('..', '.'));
                
                if($file_list){
                    if ($zip->open($zipName, \ZipArchive::CREATE) === true) {
                        foreach ($file_list as $file) {
                            $zip->addFile($localDirectory.$file, $file);
                        }
                        $zip->close();
                        return response()->download($zipName, 'bandListMonth_'.$start.'.zip');
                    }
                }else{
                    return "No existen eventos";
                }
        }
    }
    
    /*Dado una serie de parametros, generamos una lista de archivos pdf, que guardaremos en local, para después generar un zip que contenha todo ello*/
    public function getListPdf(Request $request,$start, $end){
        $rules = array(
        'start'             => 'required|date_format:"Y-m-d',
        'end'            => 'required|date_format:"Y-m-d'
        );
        
        $urlParam = [
        'start' => $start,
        'end' => $end,
        ];
        
        $validator = Validator::make($urlParam, $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return $messages;
            
        }else{
            //eliminamos temporales archivos generados en anteriores peticiones
            $localDirectory = "../storage/pdf/pdfBands/";
            $zipName = "../storage/pdf/pdfBands.zip";
            $file_list = array_diff(scandir($localDirectory), array('..', '.'));
            
            //eliminos los pdf
            foreach($file_list as $file){
                unlink($localDirectory.$file);
            }
            //eliminamos el zip final
            if (file_exists($zipName)) {
                unlink($zipName);
            }
            
            //obtenemos un listado de todos los hoteles junto sus respectivos eventos para dicho mes
            $bandList = Band::getBandsByTime($urlParam);
            
            //preparamos los parámetros para generar los pdf
            \Log::info($bandList);
            foreach($bandList as $band){
                
                $htmlParams = [
                'start' => $start,
                'end' => $end,
                'events' => $band->events,
                'artist' => $band
                ];
                
                //guardamos los pdf
                $pdf = PDF::loadView('calendarEvents', $htmlParams)->setPaper('a4', 'landscape');
                $pdf->save($localDirectory.$band->name.'_'.$start.'.pdf');
            }
            //Generamos el zip ha descargar
            $zip = new \ZipArchive();
            $file_list = array_diff(scandir($localDirectory), array('..', '.'));
            
            if($file_list){
                if ($zip->open($zipName, \ZipArchive::CREATE) === true) {
                    foreach ($file_list as $file) {
                        $zip->addFile($localDirectory.$file, $file);
                    }
                    
                    $zip->close();
                    //forzamos las descarga del zip
                    //header("Content-type: application/octet-stream");
                    //header("Content-disposition: attachment; filename=locationsList_".$start.".zip");
                    // readfile($zipName);
                    return response()->download($zipName, 'bandList_'.$start.'.zip');
                }
            }else{
                return "No existen eventos";
            }
        }
    }
    
    public function createPdf(Request $request, $id, $start, $end){
        
        $rules = array(
        'id'            => 'required|numeric',
        'start'             => 'required|date_format:"Y-m-d',
        'end'            => 'required|date_format:"Y-m-d'
        );
        
        $urlParam = [
        'start' => $start,
        'end' => $end,
        'id' => $id
        ];
        
        
        $validator = Validator::make($urlParam, $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return $messages;
            
        }else{
            $band = Band::findOrFail($id);
            $params = [
            'start' => $start,
            'end' => $end
            ];
            
            $events = $band->getEvents($params);
            
            $htmlParams = [
            'start' => $start,
            'end' => $end,
            'events' => $events,
            'artist' => $band
            ];
            
            $pdf = PDF::loadView('calendarEvents', $htmlParams)->setPaper('a4', 'landscape');
            //return $pdf->stream('xarxaMusical.pdf');
            return $pdf->download($band->name.'_'.$start.'.pdf');
        }
        
    }
    
    
    /**
    * Return the bands
    *
    * @return Response
    */
    public function index()
    {
        return  Band::all();
        
    }
    
    
    /**
    * Return the band
    *
    * @return Response
    */
    public function delete(Request $request, $id)
    {
        Band::destroy($id);
    }
    
    
    public function checkOverlapEvents(Request $request, $id){
        $this->validate($request, [
        'start' => 'required|date_format:"Y-m-d',
        'end' => 'required|date_format:"Y-m-d'
        ]);
        
        $params = [
        'start' => $request->input('start'),
        'end' => $request->input('end'),
        ];
        $band = Band::findOrFail($id);
        return $band->checkOverlapEvents($params);
    }
    
    /**
    * Return the events
    *
    * @return Response
    */
    public function getEvents(Request $request, $id){
        $this->validate($request, [
        'start' => 'required|date_format:"Y-m-d',
        'end' => 'required|date_format:"Y-m-d'
        ]);
        
        $params = [
        'start' => $request->input('start'),
        'end' => $request->input('end'),
        ];
        $band = Band::findOrFail($id);
        return $band->getEventsCalendar($params);
        
    }
    
    
    
    /**
    * Return the Band
    *
    * @return Response
    */
    public function show(Request $request, $id)
    {
        $band = Band::findOrFail($id);
        return  $band->show();
    }
    
    /**
    * Return the Band
    *
    * @return Response
    */
    public function create(Request $request)
    {
        $this->validate($request, [
        'name' => 'required|string',
        'artists' => 'required|array'
        ]);
        
        $params = [
        'name' => $request->input('name'),
        'artists' => $request->input('artists')
        ];
        return Band::createBand($params);
    }
    
    public function assignArtist(Request $request, $id)
    {
        $this->validate($request, [
        'artist' => 'required|numeric',
        'id' => 'required|numeric'
        ]);
        
        $params = [
        'artist' => $request->input('artist')
        ];
        $band = Band::findOrFail($id);
        return $band->assignArtist($params);
        
    }
    
    public function deleteArtist(Request $request)
    {
        $this->validate($request, [
        'artist' => 'required|numeric',
        'id' => 'required|numeric'
        ]);
        
        $params = [
        'artist' => $request->input('artist'),
        'id' => $request->input('id')
        ];
        $band = Band::findOrFail($request['id']);
        return $band->deleteArtist($params);
        
    }
    
    
    /**
    * Return the band
    *
    * @return Response
    */
    public function updateBand(Request $request, $id)
    {
        $this->validate($request, [
        'name' => 'required|string'
        ]);
        
        $params = [
        'name' => $request->input('name')
        ];
        $band = Band::findOrFail($id);
        return $band->updateBand($params);
    }
    
    
}