<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

Route::group(['prefix' => 'api'], function()
{
	//Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);

	//artist
	Route::get('artist', 'ArtistController@index');
	Route::put('artist', 'ArtistController@create');
	Route::get('artist/{id}', 'ArtistController@show');
	Route::post('artist/{id}', 'ArtistController@updateArtist');
	Route::get('artist/{id}/events', 'ArtistController@getEvents');
	Route::delete('artist/{id}', 'ArtistController@delete');
	Route::get('artistPDF/{id}/{start}/{end}', 'ArtistController@createPdf');
	Route::get('artist/{id}/checkOverlapEvents', 'ArtistController@checkOverlapEvents');

	//Band
	Route::get('band', 'BandController@index');
	Route::put('band', 'BandController@create');
	Route::get('band/{id}', 'BandController@show');
	Route::post('band/{id}', 'BandController@updateBand');
	Route::get('band/{id}/events', 'BandController@getEvents');
	Route::delete('band/{id}', 'BandController@delete');
	Route::get('bandPDF/{id}/{start}/{end}', 'BandController@createPdf');
	Route::get('band/{id}/checkOverlapEvents', 'BandController@checkOverlapEvents');
	Route::post('artistband/{id}', 'BandController@assignArtist');
	Route::post('artistband', 'BandController@deleteArtist');
	Route::get('bandListPDF/{start}/{end}', 'BandController@getListPdf');
	Route::get('bandListPDFMonth/{start}/{end}', 'BandController@getListMonthPdf');

	//event
	Route::get('event', 'EventController@index');
	Route::get('event/{id}', 'EventController@show');
	Route::post('event/{id}', 'EventController@updateEvent');
	Route::put('event', 'EventController@create');
	Route::post('eventband/{id}', 'EventController@assignBand');
	Route::post('eventband', 'EventController@deleteBand');
	Route::delete('event/{id}', 'EventController@delete');

	//location
	Route::get('location', 'LocationController@index');
	Route::get('location/{id}', 'LocationController@show');
	Route::get('location/{id}/events', 'LocationController@getEvents');
	Route::post('location/{id}', 'LocationController@update');
	Route::put('location', 'LocationController@create');
	Route::get('locationPDF/{id}/{start}/{end}/{needCost}', 'LocationController@createPdf');
	Route::get('locationListPDF/{start}/{end}/{needCost}', 'LocationController@getListPdf');

	//city
	Route::get('city', 'CityController@index');
	Route::get('city/{id}', 'CityController@show');
	Route::post('city/{id}', 'CityController@update');
	Route::put('city', 'CityController@create');

	//user
	Route::post('user/{id}', 'AuthenticateController@update');
	Route::post('authenticate', 'AuthenticateController@authenticate');
	Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');
});
