<?php

namespace App\model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\model\Event;

class Location extends Model
{
    
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'location';
    
    
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['name', 'address','city_id'];
    
    
    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = [];

     public static function getObjectlocation($params){
        $location = new Location();
        $location->name = $params->name;
        $location->address = $params->address;
        $location->city_id = $params->city_id;
        $location->id = $params->id;
        return $location;

    }

    public static function createLocation($params){
        $location = new Location();
        $location->name = $params['name'];
        $location->address = $params['address'];
        $location->city_id = $params['city_id'];
        $location->save();
        $location;
    }
    public function updateLocation ($params){
        $this->name = $params['name'];
        $this->address = $params['address'];
        $this->city_id = $params['city_id'];
        $this->save();
        return $this;
    }
    
    public function city(){
        return DB::table('location as l')
        ->join('city as c', 'l.city_id','=','c.id')
        ->where('l.id','=',$this->id)
        ->select('c.*')
        ->first();
    }
    
    public function getEventsCalendar($params){
        $eventList = DB::table('event as e')
        ->join('location as l', 'l.id','=','e.location_id')
        ->where('start_day','>=' ,$params['start'])
        ->where('end_day','<=' ,$params['end'])
        ->where('l.id','=',$this->id)
        ->select('e.*','l.name as locationName',DB::raw('CONCAT_WS("T", e.start_day,e.start_hour) as start'),DB::raw('CONCAT_WS("T",e.end_day,e.end_hour) as end'))
        ->get();
        
        foreach($eventList as &$event){
            /*$e = Event::findOrFail($event->id);*/
            $e = Event::getObjectEvent($event); //hacemos esto para evitar hacer consultas constante a la bbdd, ya que, de la operación anterior ya tenemos todos los datos actualizados.
            
            //Obtenemos solo los nombres de los aristas
            $groupList =  array_map(function($e) {
                return is_object($e) ? $e->name : $e['name'];
            }, $e->band());
            
            //concatenamos el array para devolver un string (es para mostrar en los calendarioss)
            //los artistas es lo que se va a mostrar en el calendario, por eso lo llamamos text, para que el calendario de angularjs lo coja como titulo.
            $nameArtist = implode(" , ",$groupList);
            $event->text = $nameArtist;
        }
        return $eventList;
    }
    /*Obtenemos un listado de todos los hoteles que trabajan dado una fecha de tiempo*/
    public static function getLocationsByTime($params){
        $hotelList =  DB::table('event as e')
        ->leftJoin('location as l', 'l.id','=','e.location_id')
        ->where('start_day','>=' ,$params['start'])
        ->where('end_day','<=' ,$params['end'])
        ->select('l.*')
        ->groupBy('l.id')
        ->get();
        
        foreach($hotelList as &$hotel){
            $e = Location::getObjectlocation($hotel);
            $hotel->events = $e->getEvents($params);
        }
        return $hotelList;
    }
    /*Funcion especifica para obtener los eventos para generar el pdf*/
    public function getEvents($params){
        $eventList =  DB::table('event as e')
        ->join('location as l', 'l.id','=','e.location_id')
        ->where('start_day','>=' ,$params['start'])
        ->where('end_day','<=' ,$params['end'])
        ->where('l.id','=',$this->id)
        ->select('e.*')
        ->orderBy('start_hour', 'ASC')
        ->get();
        
        foreach($eventList as &$event){
            $e = Event::getObjectEvent($event);
            $event->artists = $e->band();
        }
        return $eventList;
        
    }
    public function showLocation(){
        $result = [
        'location' => $this,
        'city' => $this->city()
        ];
        return $result;
    }
    public static function getAll(){
        return DB::table('location as l')
        ->join('city as c', 'l.city_id','=','c.id')
        ->select('l.id' ,'l.name','l.address', 'c.name as cityName' ,'c.code')
        ->get();
    }
}