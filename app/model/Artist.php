<?php

namespace App\model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\model\Band;
use App\model\Artist_band;

class Artist extends Model
{
    
    
    
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'artist';
    
    
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['name', 'email','telf' ,'created_at','updated_at'];
    
    
    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = [];
    static function createArtist($params){
        $artist = new Artist();
        $artist->name = $params['name'];
        $artist->telf = $params['telf'];
        $artist->email = $params['email'];
        $artist->save();
        
        //por defecto al crear un artista se va a crear una banda con el mismo nombre, que lo eventos trabajaran siempre con bandas
        $bandParams = [
            "name" => $params['name'],
            "artists" => array($artist->id)
        ];
        $band_id = Band::createBand($bandParams); //creamos un grupo con el mismo nombre y los relacionamos
        return $artist->id;
    }
    
    public function assingArtistBand($params){
        return Artist_band::createArtistBand($params);
    }
    
    
    function updateArtist($params){
        $this->name = $params['name'];
        $this->telf = $params['telf'];
        $this->email = $params['email'];
        $this->save();
        return $this;
    }
    
    function getBands(){
        return DB::table('artist_band as ab')
        ->join('band as b', 'ab.band_id','=','b.id')
        ->where('ab.artist_id','=',$this->id)
        ->select('b.*')
        ->get();
    }
    function show(){
        return [
            'artist' => $this,
            'bands' => $this->getBands()
        ];
    }
    
    /*Esta funcion es especifica para mostrar los eventos para el calendario de angularjs*/
    public function getEventsCalendar($params){
        $eventList =  DB::table('artist_band as ab')
        ->join('band as b', 'ab.band_id','=','b.id')
        ->join('band_event as be', 'be.band_id','=','b.id')
        ->join('event as e', 'be.event_id','=','e.id')
        ->join('location as l', 'l.id','=','e.location_id')
        ->where('e.start_day','>=' ,$params['start'])
        ->where('e.end_day','<=' ,$params['end'])
        ->where('ab.artist_id','=',$this->id)
        ->select('e.*',DB::raw('CONCAT_WS("T", e.start_day,e.start_hour) as start'),DB::raw('CONCAT_WS("T",e.end_day,e.end_hour) as end'))
        ->get();
        
        foreach($eventList as &$event){
            /*$e = Event::findOrFail($event->id);*/
            $e = Event::getObjectEvent($event); //hacemos esto para evitar hacer consultas constante a la bbdd, ya que, de la operación anterior ya tenemos todos los datos actualizados.
            
            //Obtenemos solo los nombres de los aristas
            $bandtList =  array_map(function($e) {
                return is_object($e) ? $e->name : $e['name'];
            }, $e->band());
            
            //concatenamos el array para devolver un string (es para mostrar en los calendarioss)
            //los artistas es lo que se va a mostrar en el calendario, por eso lo llamamos text, para que el calendario de angularjs lo coja como titulo.
            $nameArtist = implode(" , ",$bandtList);
            $event->text = $nameArtist;
        }
        return $eventList;
    }
    
    /*Obtenemos todos los eventos definidos entre dos margenes*/
    public function getEvents($params){
        return DB::table('artist_band as ab')
        ->join('band as b', 'ab.band_id','=','b.id')
        ->join('band_event as be', 'be.band_id','=','b.id')
        ->join('event as e', 'be.event_id','=','e.id')
        ->join('location as l', 'l.id','=','e.location_id')
        ->join('city as c', 'c.id','=','l.city_id')
        ->where('e.start_day','>=' ,$params['start'])
        ->where('e.end_day','<=' ,$params['end'])
        ->where('ab.artist_id','=',$this->id)
        ->select('e.*','l.*','c.name as city')
        ->orderBy('start_hour', 'ASC')
        ->get();
    }
    
    /*Devuelve todos los días donde un artistas tenga más de dos eventos*/
    public function checkOverlapEvents($params){
        return DB::table('artist_band as ab')
        ->join('band as b', 'ab.band_id','=','b.id')
        ->join('band_event as be', 'be.band_id','=','b.id')
        ->join('event as e', 'be.event_id','=','e.id')
        ->where('e.start_day','>=' ,$params['start'])
        ->where('e.end_day','<=' ,$params['end'])
        ->where('ab.artist_id','=',$this->id)
        ->select('e.start_day',DB::raw('count(*) as count'))
        ->groupBy('e.start_day')
        ->having('count', '>', 1)
        ->get();
    }
}