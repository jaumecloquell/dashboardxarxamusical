<?php

namespace App\model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Band_event extends Model
{
    
    
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'band_event';
    
    
    
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['band_id', 'event_id','created_at','updated_at'];
    
    
    
    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = [];

    public static function createBandEvent($params){
        if(!Band_event::exist($params)){
            $ae = new Band_event();
            $ae->band_id = $params['band_id'];
            $ae->event_id = $params['event_id'];
            return $ae->save();
        }
    }
    private static function exist($params){
        $result =  DB::table('band_event')
        ->where('band_id','=',$params['band_id'])
        ->where('event_id','=',$params['event_id'])
        ->first();
        if($result){
            return true;
        }else{
            return false;
        }
        
    }
    public static function deleteBandEvent($params){
        \Log::info($params);
        return DB::table('band_event')
        ->where('band_id','=',$params['band_id'])
        ->where('event_id','=',$params['event_id'])
        ->delete();
    }
}