<?php

namespace App\model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Artist_band extends Model
{
    
    
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'artist_band';
    
    
    
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['artist_id', 'band_id','created_at','updated_at'];
    
    
    
    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = [];
    
    public static function createArtistBand($params){
        if(!Artist_band::exist($params)){
            $ae = new Artist_band();
            $ae->artist_id = $params['artist_id'];
            $ae->band_id = $params['band_id'];
            return $ae->save();
        }
    }

    private static function exist($params){
        $result =  DB::table('artist_band')
        ->where('artist_id','=',$params['artist_id'])
        ->where('band_id','=',$params['band_id'])
        ->first();
        if($result){
            return true;
        }else{
            return false;
        }
        
    }
    public static function deleteArtistBand($params){
        return DB::table('artist_band')
        ->where('artist_id','=',$params['artist_id'])
        ->where('band_id','=',$params['band_id'])
        ->delete();
    }
}