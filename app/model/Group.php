<?php

namespace App\model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Group extends Model 
{
	
	/**
	* The database table used by the model.
	     *
	     * @var string
	     */
	    protected $table = 'group';
	
	
	/**
	* The attributes that are mass assignable.
	     *
	     * @var array
	     */
	    protected $fillable = ['created_at','updated_at'];
	
	
	/**
	* The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    public static function createGroup(){
       $group = new Group();
       $group->save();
       return $group;
    }
}
