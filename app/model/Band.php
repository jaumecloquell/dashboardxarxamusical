<?php

namespace App\model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Band extends Model
{
    
    
    
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'band';
    
    
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['name','created_at','updated_at'];
    
    
    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = [];
    
    static function createBand($params){
        $band = new Band();
        $band->name = $params['name'];
        $band->save();
        $band->assignListArtist($params);
        return $band->id;
    }
    
    public function assignListArtist($params){
        foreach ($params['artists'] as $artist){
            $assignParam = [
            'artist_id' => $artist,
            'band_id' => $this->id
            ];
            Artist_band::createArtistBand($assignParam);
        }
        return "ok";
    }
    
    
    public function assignArtist($params){
        $assignParam = [
        'artist_id' => $params['artist'],
        'band_id' => $this->id
        ];
        Artist_band::createArtistBand($assignParam);
        return "ok";
    }
    
    public function deleteArtist($params){
        $assignParam = [
        'artist_id' => $params['artist'],
        'band_id' => $this->id
        ];
        Artist_band::deleteArtistBand($assignParam);
        return "ok";
    }
    
    function updateBand($params){
        $this->name = $params['name'];
        $this->save();
        return $this;
    }
    
    function show(){
        return [
        'band' => $this,
        'artists' => $this->artists(),
        'restArtists' => $this->getOthersArtists()
        ];
    }
    
    function artists(){
        return DB::table('artist_band as ab')
        ->join('artist as a', 'ab.artist_id','=','a.id')
        ->where('ab.band_id','=',$this->id)
        ->select('a.*')
        ->get();
    }
    
    public function getOthersArtists(){
        return Artist::WhereNotIn('id', function($query){
            $query->select('artist_id')
            ->from('artist_band')
            ->where('band_id','=',$this->id);
        })->get();
    }
    
    /*Esta funcion es especifica para mostrar los eventos para el calendario de angularjs*/
    public function getEventsCalendar($params){
        $eventList =  DB::table('band_event as be')
        ->join('event as e', 'be.event_id','=','e.id')
        ->join('location as l', 'l.id','=','e.location_id')
        ->where('e.start_day','>=' ,$params['start'])
        ->where('e.end_day','<=' ,$params['end'])
        ->where('be.band_id','=',$this->id)
        ->select('e.*','l.name as locationName',DB::raw('CONCAT_WS("T", e.start_day,e.start_hour) as start'),DB::raw('CONCAT_WS("T",e.end_day,e.end_hour) as end'))
        ->get();
        
        foreach($eventList as &$event){
            /*$e = Event::findOrFail($event->id);*/
            $e = Event::getObjectEvent($event); //hacemos esto para evitar hacer consultas constante a la bbdd, ya que, de la operación anterior ya tenemos todos los datos actualizados.
            
            //Obtenemos solo los nombres de los aristas
            $bandList =  array_map(function($e) {
                return is_object($e) ? $e->name : $e['name'];
            }, $e->band());
            
            //concatenamos el array para devolver un string (es para mostrar en los calendarioss)
            //los artistas es lo que se va a mostrar en el calendario, por eso lo llamamos text, para que el calendario de angularjs lo coja como titulo.
            $nameArtist = implode(" , ",$bandList);
            $event->text = $nameArtist;
        }
        return $eventList;
    }

    public static function getBandsByTime($params){
        $bandlist =  DB::table('band_event as be')
        ->join('event as e', 'be.event_id','=','e.id')
        ->where('e.start_day','>=' ,$params['start'])
        ->where('e.end_day','<=' ,$params['end'])
        ->groupBy('be.band_id')
        ->get();

        foreach($bandlist as &$band){
            $band = Band::findOrFail($band->band_id);
            $band->events = $band->getEvents($params);
        }

        return $bandlist;
    }
    
    /*Obtenemos todos los eventos definidos entre dos margenes*/
    public function getEvents($params){
        return DB::table('band_event as be')
        ->join('event as e', 'be.event_id','=','e.id')
        ->join('location as l', 'l.id','=','e.location_id')
        ->join('city as c', 'c.id','=','l.city_id')
        ->where('start_day','>=' ,$params['start'])
        ->where('end_day','<=' ,$params['end'])
        ->where('be.band_id','=',$this->id)
        ->select('e.*','l.*','c.name as city')
        ->orderBy('start_hour', 'ASC')
        ->get();
    }
    
    /*Devuelve todos los días donde un artistas tenga más de dos eventos*/
    public function checkOverlapEvents($params){
        return DB::table('band_event as be')
        ->join('event as e', 'be.event_id','=','e.id')
        ->where('e.start_day','>=' ,$params['start'])
        ->where('e.end_day','<=' ,$params['end'])
        ->where('be.band_id','=',$this->id)
        ->select('e.start_day',DB::raw('count(*) as count'))
        ->groupBy('e.start_day')
        ->having('count', '>', 1)
        ->get();
    }
}