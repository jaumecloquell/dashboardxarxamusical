<?php

namespace App\model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\model\Band_event;
use App\model\Band;

class Event extends Model
{
    
    
    
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'event';
    
    
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['location_id','group_id','start_day','end_day','external_description','internal_description','cost','start_hour','end_hour', 'created_at','updated_at'];
    // 	si añadimos un nuevo atributo debemos, tambien modificar la funcion updateRestEvents()!!!!!
    
    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = [];
    
    /*Funcion que te crea un objecto Evento sin necesidad de hacer una consulta en la base de datos*/
    public static function getObjectEvent($params){
        $event = new Event();
        $event->group_id =  $params->group_id;
        $event->location_id =  $params->location_id; 
        $event->start_day = $params->start_day;
        $event->end_day = $params->end_day;
        $event->start_hour = $params->start_hour;
        $event->end_hour = $params->end_hour;
        $event->external_description = $params->external_description;
        $event->internal_description = $params->internal_description;
        $event->cost = $params->cost;
        $event->id = $params->id;
        return $event;

    }
    
    public static function createEvent($params){
        $group = Group::createGroup();
        $result = [];
        foreach($params['dates'] as $date){
            $event = new Event();
            $event->group_id = $group->id;
            $event->location_id = $params['location'];
            $event->start_day = $date;
            $event->end_day = $date;
            $event->start_hour = $params['start_hour'];
            $event->end_hour = $params['end_hour'];
            $event->external_description = $params['external_description'];
            $event->internal_description = $params['internal_description'];
            $event->cost = $params['cost'];
            $event->save();
            
            foreach($params['bands'] as $band){
                $param = [
                'band_id' => $band,
                'event_id' => $event->id
                ];
                Band_event::createBandEvent($param);
            }
            array_push($result, $event->id);
        }
        return $result;
    }
    
    /*Asigna los artistas de todos los eventos posterior e igual al actual evento*/
    public function assignRestBand($params){
        $eventList = $this->getEventsByGroup();
        foreach($eventList as $event){
            $params['event_id'] = $event->id;
            Band_event::createBandEvent($params);
        }
        return "ok";
    }
    public function assignBand($params){
        return Band_event::createBandEvent($params);
    }
    
    /*Elimina los artistas de todos los eventos posterior e igual al actual evento*/
    public function deleteRestBand($params){
        $eventList = $this->getEventsByGroup();
        foreach($eventList as $event){
            $params['event_id'] = $event->id;
            Band_event::deleteBandEvent($params);
        }
        return "ok";
    }
    
    public function deleteBand($params){
        return Band_event::deleteBandEvent($params);
    }
    
    /*Esta funcion obtiene solos las bandas del evento*/
    public function band(){
        return DB::table('band_event as ae')
        ->join('band as b', 'b.id','=' ,'ae.band_id')
        ->where('ae.event_id','=' ,$this->id)
        ->select('b.*')
        ->get();
    }
    
    /*Obtenemos todos las bandas que no han sido asignados a dicho evento*/
    public function getOthersBands(){
        return Band::WhereNotIn('id', function($query){
            $query->select('band_id')
            ->from('band_event')
            ->where('event_id','=',$this->id);
        })->get();
    }
    
    /*Obtiene todos los eventos posteriores restantes de un mismo grupo*/
    private function getEventsByGroup(){
        return DB::table('event')
        ->where('group_id','=' ,$this->group_id)
        ->where('start_day','>=' ,$this->start_day)
        ->select('*')
        ->get();
    }
    
    /*Eliminamos todos los eventos posterior o igual al acutal evento*/
    public function deleteRestEvents(){
        $eventList = $this->getEventsByGroup();
        foreach($eventList as $event){
            Event::destroy($event->id);
        }
        return "ok";
    }
    
    /*Actualizamos todos los eventos a posteriori al actual evento*/
    public function updateRestEvents($params){
        //generamos un array con todos lo parametros que han sido modificados. No comprobamos ni start_day ni end_day
        $paramsToUpdate = array();
        $attributeList = ['location_id','external_description','internal_description','cost','start_hour','end_hour'];
        foreach($attributeList as $attribute){
            if($this->$attribute != $params[$attribute]){
                array_push($paramsToUpdate, $attribute);
            }
        }
        $eventList = $this->getEventsByGroup();
        foreach($eventList as $event){
            $e = Event::findOrFail($event->id);
            foreach ($paramsToUpdate as $attribute) {
                $e->$attribute = $params[$attribute];
                $e->save();
            }
        }
        return "ok";
    }
    
    public function updateEvent($params){
        $this->start_day = $params['start_day'];
        $this->end_day = $params['end_day'];
        $this->start_hour = $params['start_hour'];
        $this->end_hour = $params['end_hour'];
        $this->cost = $params['cost'];
        $this->internal_description = $params['internal_description'];
        $this->external_description = $params['external_description'];
        $this->location_id = $params['location_id'];
        $this->save();
        return $this;
    }
    
    public static function getEventsCalendar($params){
        $eventList =  DB::table('event as e')
        ->join('location as l', 'l.id','=','e.location_id')
        ->where('e.start_day','>=' ,$params['start'])
        ->where('e.end_day','<=' ,$params['end'])
        ->select('e.*','l.name as locationName', DB::raw('CONCAT_WS("T", e.start_day,e.start_hour) as start'),DB::raw('CONCAT_WS("T",e.end_day,e.end_hour) as end'))
        ->get();
        
        foreach($eventList as &$event){
           /*$e = Event::findOrFail($event->id);*/
            $e = Event::getObjectEvent($event); //hacemos esto para evitar hacer consultas constante a la bbdd, ya que, de la operación anterior ya tenemos todos los datos actualizados.

            //Obtenemos solo los nombres de los aristas
            $bandList =  array_map(function($e) {
                return is_object($e) ? $e->name : $e['name'];
            }, $e->band());

            //concatenamos el array para devolver un string (es para mostrar en los calendarioss)
            //los artistas es lo que se va a mostrar en el calendario, por eso lo llamamos text, para que el calendario de angularjs lo coja como titulo.
            $nameArtist = implode(" , ",$bandList);
            $event->text = $nameArtist;
        }
        return $eventList;
    }
    
    
    public static function getEventsByRange($params){
        return DB::table('event')
        ->where('start_day','>=' ,$params['start'])
        ->where('end_day','<=' ,$params['end'])
        ->select('*')
        ->get();
    }
    
    public function show(){
        $results = [
        'event' => $this,
        'band' => $this->band(),
        'location' => $this->location(),
        'restBand' => $this->getOthersBands()
        ];
        return $results;
    }
    
    public function location(){
        return DB::table('event as e')
        ->join('location as l', 'e.location_id','=' ,'l.id')
        ->join('city as c', 'l.city_id','=','c.id')
        ->where('e.id','=' ,$this->id)
        ->select('l.name as locationName','l.address','c.name as city','c.code','l.id')
        ->first();
    }
}