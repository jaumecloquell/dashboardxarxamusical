<?php

namespace App\model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class City extends Model 
{
	
	/**
	* The database table used by the model.
	     *
	     * @var string
	     */
	    protected $table = 'city';
	
	
	/**
	* The attributes that are mass assignable.
	     *
	     * @var array
	     */
	    protected $fillable = ['name', 'code','created_at','updated_at'];
	
	
	/**
	* The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    public static function createCity($params){
       $city = new City();
       $city->name = $params['name'];
       $city->code = $params['code'];
       $city->save();
       return $city;
    }
}
